FROM buildkite/puppeteer:latest

COPY ./puppeteer-cluster /puppeteer-cluster

# Install Puppeteer under /node_modules so it's available system-wide
RUN yarn add puppeteer-cluster

ENV  PATH="${PATH}:/node_modules/.bin"