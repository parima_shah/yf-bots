"use strict"; 

let puppeteer = require("puppeteer");
let { Cluster } = require("puppeteer-cluster");


const links = [
    "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=c8ee3fcc-2dda-4a26-b13f-8b5ccfb6b36a",
    "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=6652a07c-576c-422a-b7de-fdcd1627cb94",
    "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=ce336201-779b-42f8-a759-45b20a069891",
    "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=02360587-5928-4884-965f-42eb219679b7",
    "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=2ab6149e-07b0-426d-922d-ee083cd18d3a",
    "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=c192e0a8-6e5d-4ed7-8643-79358338ea83",
    "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=58392ef8-2e99-42ae-8bae-7039be1bc716",
    "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=eb3d6113-f58a-4264-89d0-86268d8b2a7d",
    "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=f2454f6a-e153-44eb-934f-ba4959e1ed4b"
];

//sed -i '' "s/xxx/081cdcfa660540b2cfe9a5bf2e0ff2a76fc6123e570e790267/g" "$(brew --prefix)"/etc/buildkite-agent/buildkite-agent.cfg


(async () => {
  //Create cluster with 10 workers
  const cluster = await Cluster.launch({
    concurrency: Cluster.CONCURRENCY_CONTEXT,
    maxConcurrency: 10,
    monitor: true,
    timeout: 500000
  });
  // Print errors to console
  cluster.on("taskerror", (err, data) => {
    console.log(`Error crawling ${data}: ${err.message}`);
    console.log(`${err.message}`);
  });
  // Dumb sleep function to wait for page load
  async function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  await cluster.task(async ({ page, data: url, worker }) => {
   
    await page.goto(url);
    await timeout(20000);
    const text = await page.evaluate(() => Array.from(document.querySelectorAll('button'), element => element.textContent));
    console.log(text);
    const index = text.findIndex(x => x.includes('Join without'));
    const buttons = await page.$$('button');
    await buttons[index].click();
    await timeout(2000);

    const liveSessionText = await page.evaluate(() => Array.from(document.querySelectorAll('button'), element => element.textContent));
    console.log(liveSessionText);
    const liveSessionIndex = liveSessionText.findIndex(x => x.includes('Join'));
    console.log(liveSessionIndex);
    const liveSessionButtons = await page.$$('button');
    await liveSessionButtons[liveSessionIndex].click();

    await timeout( 60 * 1000);

  });


  // *************  ENQUEUE ***********************/
  for (let i = 0; i < 10; i++) {
    cluster.queue(links[i]);
  }
  await cluster.idle();
  await cluster.close();
})();


