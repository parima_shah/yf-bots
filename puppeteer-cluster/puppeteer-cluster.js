"use strict"; 

let puppeteer = require("puppeteer");
let { Cluster } = require("puppeteer-cluster");
let { allLinks } = require("./links.js");

console.log(process.env.BUILDKITE_PARALLEL_JOB);

const concurrency = 50;
//var myArgs = process.argv.slice(2);
const offset = (process.env.BUILDKITE_PARALLEL_JOB * concurrency) || 0;  //(myArgs[0] * concurrency) || 0;
const links = allLinks.slice(offset);
console.log(links[0]);

//sed -i '' "s/xxx/081cdcfa660540b2cfe9a5bf2e0ff2a76fc6123e570e790267/g" "$(brew --prefix)"/etc/buildkite-agent/buildkite-agent.cfg

(async () => {
  //Create cluster with 10 workers
  const cluster = await Cluster.launch({
    concurrency: Cluster.CONCURRENCY_CONTEXT,
    maxConcurrency: concurrency,
    timeout: 60 * 50 * 1000,
    puppeteerOptions: { args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage'] },
  });
  // Print errors to console
  cluster.on("taskerror", (err, data) => {
    console.log(`Error crawling ${data}: ${err.message}`);
  });
  // Dumb sleep function to wait for page load
  async function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  await cluster.task(async ({ page, data: url, worker }) => {

    await page.setDefaultNavigationTimeout(0); 
    await page.goto(url, {
      waitUntil: 'load',
      timeout: 0
    });
    await timeout(120 * 1000); // 90 seconds
    const text = await page.evaluate(() => Array.from(document.querySelectorAll('button'), element => element.textContent));
    const index = text.findIndex(x => x.includes('Join without'));
    const buttons = await page.$$('button');
    await page.setDefaultNavigationTimeout(0); 
    try{
      console.log("logging in",  worker.id)
      await buttons[index].click();
    }
    catch(e){
      console.error("Unable to login", e);
      throw e;
    }
    // console.log("logged in",  worker.id);
    // await timeout(30 * 1000);
    // console.log("about to click the session",  worker.id);
    // const liveSessionText = await page.evaluate(() => Array.from(document.querySelectorAll('button'), element => element.textContent));
    // console.log("evaluated all buttons", worker.id);
    // const liveSessionIndex = liveSessionText.findIndex(x => x.includes('Join'));
    // console.log("Found the button",  worker.id);
    // const liveSessionButtons = await page.$$('button');
    // // // console.log("just finished timeout");
    // // // //await page.screenshot({ path: `screenshot${worker.id}.png` });
    // // // await page.waitForSelector('button.sc-fzpisO', {
    // // //   timeout: 0
    // // // });
    //  console.log('got it!', worker.id);
    // try{
    //   //await page.click('button.sc-fzpisO'); //sc-fzpisO lorwkL
    //   await liveSessionButtons[liveSessionIndex].click();
    //   console.log("clicked join session!", worker.id);
    // }
    // catch(e){
    //   console.error("Unable to join the session", e);
    //   throw e;
    // }
    // // await page.waitForNavigation();
    
    console.log("waiting a minute...", worker.id)
    await timeout(60000);
    //console.log("getting screenshot")
    //await page.screenshot({ path: `screenshot$.png` });
    console.log("waiting for button", worker.id);
    await page.waitForSelector('button.sc-fzpisO', {
      timeout: 0
    });
    console.log("clicking button")
    await page.click('button.sc-fzpisO', worker.id);
    console.log("done clicking button")
    
    
    await timeout( 50 * 60 * 1000);
  });


  // *************  ENQUEUE ***********************/
  for (let i = 0; i < concurrency; i++) {
    cluster.queue(links[i]);
  }
  await cluster.idle();
  // for (let i = 0; i < concurrency; i++) {
  //   cluster.queue(links[i]);
  // }
  // await cluster.idle();
  await cluster.close();
})();


