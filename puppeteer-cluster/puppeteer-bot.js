"use strict"; 

let puppeteer = require("puppeteer");
let { Cluster } = require("puppeteer-cluster");

// let commandLineArgs = require("command-line-args");

const ARG_DEFINITIONS = [
    {
      name: "help",
      type: Boolean,
      description: "Display this usage guide"
    },   
    {
        name: "runBot",
        type: Boolean,
        description: "runs the bot"
    },
    {
        name: "killBot",
        type: Boolean,
        description: "kills all bots",
    }
];

// const links = [
//     "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=c8ee3fcc-2dda-4a26-b13f-8b5ccfb6b36a",
//     "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=6652a07c-576c-422a-b7de-fdcd1627cb94",
//     "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=ce336201-779b-42f8-a759-45b20a069891",
//     "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=02360587-5928-4884-965f-42eb219679b7",
//     "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=2ab6149e-07b0-426d-922d-ee083cd18d3a",
//     "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=c192e0a8-6e5d-4ed7-8643-79358338ea83",
//     "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=58392ef8-2e99-42ae-8bae-7039be1bc716",
//     "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=eb3d6113-f58a-4264-89d0-86268d8b2a7d",
//     "https://www.airmeet.com/e/8bb9b720-5889-11eb-83b6-d35fdd13e50f?code=f2454f6a-e153-44eb-934f-ba4959e1ed4b"
// ];
  
async function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

(async () =>{
    // const commandOptions = commandLineArgs(ARG_DEFINITIONS);
    // const { help, runBot, killBot } = commandOptions;

    // console.log(commandOptions);
    
    let browser = await puppeteer.launch({
        args: [
          // Required for Docker version of Puppeteer
          '--no-sandbox',
          '--disable-setuid-sandbox',
          // This will write shared memory files into /tmp instead of /dev/shm,
          // because Docker’s default for /dev/shm is 64MB
          '--disable-dev-shm-usage'
        ]
      });
    const page = await browser.newPage();

    const url = "https://www.airmeet.com/e/209192b0-65d6-11eb-89d4-271711c45c6b?code=8aa58ce4-1ab7-4d67-b564-0c256b9a59ee";
    // const joinWithoutVideoSelector = '.btn.btn-primary.inverted';
    // const joinLiveSessionSelector = '.sc-fzpisO.lorwkL';
    const waitTimeout = 30000;

    await page.goto(url);
    await timeout(20000);
    const text = await page.evaluate(() => Array.from(document.querySelectorAll('button'), element => element.textContent));
    console.log(text);
    const index = text.findIndex(x => x.includes('Join without'));
    const buttons = await page.$$('button');
    await buttons[index].click();
    await timeout(2000);

    const liveSessionText = await page.evaluate(() => Array.from(document.querySelectorAll('button'), element => element.textContent));
    console.log(liveSessionText);
    const liveSessionIndex = liveSessionText.findIndex(x => x.includes('Join'));
    console.log(liveSessionIndex);
    const liveSessionButtons = await page.$$('button');
    await liveSessionButtons[liveSessionIndex].click();




    // const joinWithoutVideoButton = await page.waitForSelector(joinWithoutVideoSelector, {
    //     timeout: waitTimeout
    // });
    

    // await joinWithoutVideoButton.click();
    
    // //wait for navigation... 
    // const joinLiveSessionButton = await page.waitForSelector(joinLiveSessionSelector, {
    //     timeout: waitTimeout
    // });

    // joinLiveSessionButton.click();
    

})();

